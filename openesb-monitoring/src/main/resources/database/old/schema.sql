DROP TABLE parameter;
DROP TABLE user_account;
DROP TABLE teacher;
DROP TABLE pupil;
DROP TABLE classroom;
DROP TABLE age_group;
DROP TABLE archive_history;
DROP TABLE history;
DROP TABLE classroom_age;
DROP TABLE player_status;


CREATE TABLE parameter (
	key VARCHAR(255) NOT NULL PRIMARY KEY,
	value VARCHAR(255) NOT NULL
);

CREATE TABLE user_account(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  login VARCHAR(255),
  password  VARCHAR(255),
  ip VARCHAR(39),
  status INT, 
  active BIT DEFAULT 1
);

CREATE TABLE teacher (
  id INT NOT NULL PRIMARY KEY,
  last_name VARCHAR(255) NOT NULL,
  first_name VARCHAR(255) NOT NULL
);

CREATE TABLE pupil (
  id INT NOT NULL PRIMARY KEY,
  birthday DATETIME,
  gender VARCHAR(1) NOT NULL,
  avatar VARCHAR(255),
  id_classroom INT,
  last_name VARCHAR(255) NOT NULL,
  first_name VARCHAR(255) NOT NULL
);

CREATE TABLE classroom (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  id_teacher INT NOT NULL,
  name VARCHAR(255) NOT NULL,
);


CREATE TABLE age_group (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  age_min INT,
  age_max INT
);

CREATE TABLE archive_history (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  date DATETIME NOT NULL,
  id_pupil INT NOT NULL,
  id_jeu INT,
  action VARCHAR(255),
  additional_information VARCHAR(255)
);

CREATE TABLE history (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  date DATETIME NOT NULL,
  id_pupil INT NOT NULL,
  id_jeu INT,
  action VARCHAR(255),
  additional_information VARCHAR(255)
);



CREATE TABLE classroom_age (
	id_classroom INT NOT NULL,
	age_group INT NOT NULL
);


CREATE TABLE player_status (
	USER_ID INT NOT NULL,
	GAME_ID INT NOT NULL,
	STATUS_ID INT NOT NULL,
	LANGUAGE_ID INT NOT NULL
);




