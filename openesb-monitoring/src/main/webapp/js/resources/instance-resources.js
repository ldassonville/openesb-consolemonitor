//Instance Resource
service.factory("Instance", function ($resource) {

	var Instance =  $resource(
        "api/instance/:id",
        {id: "@id" },
        {
        	remove: {'method': 'DELETE', headers: {'Content-Type': 'application/json' }},
        	query:  {
        	           method: 'GET',
        	           isArray: true,
        	           transformResponse: function(data, header) {
        	             var wrapped = angular.fromJson(data);
        	             angular.forEach(wrapped.items, function(item, idx) {
        	                wrapped.items[idx] = new Instance(item); 
        	             });
        	           return wrapped;
        	        }
        	}
        	      
        }
    );
	Instance.prototype.loadInstance = function() {
		return "klmk";
	};
	
    return Instance;
});