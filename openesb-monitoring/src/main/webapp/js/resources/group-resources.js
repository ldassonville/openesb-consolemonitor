var service = angular.module("apiService", ["ngResource"]);

//Group Resource
service.factory("Group", function ($resource,$http) {

	var Group =  $resource(
        "api/group/:id",
        {id: "@id" },
        {
        	remove: {'method': 'DELETE', headers: {'Content-Type': 'application/json' }},
        	query:  {
        	           method: 'GET',
        	           isArray: true,
        	           transformResponse: function(data, header) {
        	             var wrapped = angular.fromJson(data);
        	             angular.forEach(wrapped.items, function(item, idx) {
        	                wrapped.items[idx] = new Group(item); 
        	             });
        	           return wrapped;
        	        }
        	}
        	      
        }
    );
	Group.prototype.loadInstances = function() {
		$http.get(this.instances,{
	        headers: {'Content-Type': 'application/json'}
	    }).
	    success(function(data, status, headers, config) {
	        return data;
	      }).
	      error(function(data, status, headers, config) {
	       alert("error");
	      });
	};
	
    return Group;
});