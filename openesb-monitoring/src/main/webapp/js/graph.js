function displayEndpointTimeline(containerId, datas){
    $('#'+containerId).highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            }
        },
        yAxis: {
            title: {
                text: 'nombre d\'appels'
            },
            min: 0
        },
        tooltip: {
            formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                    Highcharts.dateFormat('%e. %b %H:%M:%S', this.x) +': '+ this.y +' calls';
            }
        },
        plotOptions: {
        	line: {
                marker: {
                    enabled: false
                }
            }
        },
        series: [{
            name: 'reveive requests',
            color: '#00a6f3',
            data: datas[0]
        },
        {
            name: 'send reply',
            color: '#acd479',
            data: datas[1]
        },
        {
            name: 'send faults',
            color: '#fdc04d',
            data: datas[2],
            visible: false
        },
        {
            name: 'send errors',
            color: '#ff6768',
            data: datas[3],
            visible: false
        }]
    });
	
}


function displayCallRepartitionGraph(containerId, datas){
	new Highcharts.Chart({
	    chart: {
	        renderTo: containerId,
	        name: 'Calls share',
	        type: 'pie',
	    },
	    title: {
	         text: ''
	    },
	    tooltip: {
	            pointFormat: 'Percentage : <b>{point.percentage:.1f}%</b><br/>Number of calls : <b>{point.y}</b>'
	    },
	    plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                shadow: false,
	                center: ['50%', '50%'],
	                dataLabels: {
	                    enabled: true,
	                    color: '#666',
	                    connectorColor: '#666',
	                    formatter: function() {
	                        return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 2)  +' %';
	                    }
	                }
	            }
	    },
	    series: [{
	        name: 'Calls ',
	        size: '80%',
	        innerSize: '42%',
	        data: datas
	    }]
	});
}


function displayTimeColumnGraph(containerId, datas){
	
	$('#'+containerId).highcharts({
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        categories: [
	            ''
	        ]
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Duration (ms)'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                     '<td style="padding:0"><b>{point.y:.1f} ms</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    series: [{
	        name: 'Min',
	        data: [39.9],
	    	color: '#acd479'

	    }, {
	        name: 'Medium',
	        data: [63.6],
	        color: '#45bbf6'

	    }, {
	        name: 'Max',
	        data: [98.9],
	    	color: '#fdc456'

	    }]
	});	
}

