'use strict';

/* App Module */

var monitorApp = angular.module('monitorApp', [
  'ngRoute',
  'apiService',
  'instanceControllers',
  'endpointsControllers',
  'adminControllers'
]);

monitorApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/instances',{
    	  templateUrl: 'partials/instances/instance-list.html',
    	  controller: 'InstanceListCtrl'
      }).
      when('/instance/:instanceName',{
    	  templateUrl: 'partials/instances/instance-detail.html',
    	  controller: 'InstanceDetailCtrl'
      }).
      
      when('/statistics/endpoint/:endpointId', {
        templateUrl: 'partials/statistics/endpoint-statistics.html',
        controller: 'EndpointStatisticsCtrl'
      }).
      
      when('/admin/group', {
          templateUrl: 'partials/admin/group.html',
          controller: 'AdminGroupCtrl'
        }).
        
      when('/admin/instance', {
            templateUrl: 'partials/admin/instance.html',
            controller: 'AdminInstanceCtrl'
          }).
          
      otherwise({
        redirectTo: '/instances'
      });
  }]);
