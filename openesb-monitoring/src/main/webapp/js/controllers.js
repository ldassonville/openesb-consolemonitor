'use strict';

/* Controllers */

var instancesControllers = angular.module('instanceControllers', []);

instancesControllers.controller('InstanceListCtrl', ['$scope', '$http',
	function InstanceListCtrl($scope, $http) {
    	$http.get('api/instance').success(function(data) {
    		$scope.instances = data;
        });
        $scope.selection = '';
	}
]);

instancesControllers.controller('InstanceDetailCtrl', ['$scope', '$http', '$routeParams',
	function InstanceDetailCtrl($scope, $http, $routeParams) {
		$scope.instanceName = $routeParams.instanceName;
		$http.get('api/instance/name/'+$scope.instanceName).success(function(data) {
			$scope.instance = data;
			
			$http.get($scope.instance.endpoints).success(function(data){
				$scope.endpoints = data;
			});
	    });	
	}
]);



/**
 * Instance endpoints controlles 
 */
var endpointsControllers = angular.module('endpointsControllers', []);

endpointsControllers.controller('EndpointStatisticsCtrl', ['$scope', '$http', '$routeParams',
	function EndpointStatisticsCtrl($scope, $http, $routeParams) {
		$scope.endpointId = $routeParams.endpointId;
		$scope.error = '';
		
		//Get endpoint
		$http.get('api/endpoint/'+$scope.endpointId).success(function(data){
			$scope.endpoint = data;
		});
		
		//Get endpoint instance
		//$http.get($scope.endpoint).success(function(data){
		//	$scope.instance = data;
		//});
		
		//Get endpoint statistics
	    $http.get('api/endpoint/'+$scope.endpointId+'/statistics').success(function (stats) {
	    	
	    	try{
		    	Qualifier.qualify(stats);

		    	//Display endpoint timeline
		    	var timelineDatas = TimelineDataExtractor.extract(stats);
		    	displayEndpointTimeline('container', timelineDatas);
		    	
		    	//Display call state repartition
	            var callRepartitiondatas = DistributionStateDataExtractor.extract(stats);
	            displayCallRepartitionGraph('container2',callRepartitiondatas);
	            
	            // Display response time graph
	            displayTimeColumnGraph('reponseTimeContainer',{});
	    	}catch(e){
	    		if(e.code && e.code == 'NOT_DEFINED'){
	    			$scope.error = 'DATA_UNAVAILABLE';
	    		}else{
	    			$scope.error = 'UNDEFINED';
	    		}
	    	}
	    });
	}
]);

/**
 * Administration controllers.
 * Define all controllers used to administration parameter
 */
var adminControllers = angular.module('adminControllers', []);

adminControllers.controller('AdminGroupCtrl', ['$scope', '$http', 'Group',
	function AdminGroupCtrl($scope, $http, Group) {
		
		$scope.test = function(){
			var group = Group.get({},{'id':99769});
			
			$scope.testResult = group.loadInstances();
		};
		
		$scope.add = function(){
			$http.post('api/group', $scope.group,{
		        headers: {'Content-Type': 'application/json'}
		    })
		    .then(function(data, status, headers, config) {
		            $scope.groups =  Group.query();
		            $scope.reset();
		        }, 
		        function(data, status, headers, config) { // optional
		        	$scope.reset();
		        }
		    );
		};
		
		$scope.remove = function(id){
			//$http({method: 'DELETE', url: ref, data:'', headers: {'Content-Type': 'application/json'}})
			Group.remove({},{'id':id}).$promise.then(
			        //success
			        function( value ){
			            $scope.groups =  Group.query();
			            $scope.reset();
			        },
			        //error
			        function( error ){
			        	$scope.reset();
			        }
			      );
		};
	   
		$scope.reset = function(){
			$scope.group = {};
		};
		
		$scope.groups =  Group.query();
		$scope.reset();
		
	}
]);

adminControllers.controller('AdminInstanceCtrl', ['$scope', '$http',
    function AdminGroupCtrl($scope, $http) {
	$http.get('api/group').success(function(groups) {
		$scope.groups = groups;
		$scope.selectedGroup = groups[0];
		$http.get($scope.selectedGroup.instances).success(function(instances) {
			$scope.instances = instances;
		});
    });
}
]);

