var Qualifier = {
		
		qualify : function(statistics){
			
			var statistic = statistics ? statistics[0]:null; 
			if(statistics instanceof Array){
				statistic = statistics ? statistics[0]:null; 
			}else{
				statistic = statistics;
			}

			
			//Check if it's a valid statistic 
			if(!statistic){
				throw {
			       code:    "INVALID_DATA",
			       message : "Impossible to qualify data. Invalid statistics are provided"
			    };
			}else if(!statistic.lastDONETime && !statistic.lastERRORTime && !statistic.lastFaultTime ){
				throw {
			       code:     "NOT_DEFINED",
				   message : "Impossible to qualify data. This endpoint have never been called"
			    };
			}
			
			//Qualify data type
			if(!statistic.receiveRequest){
				if(statistic.sendDONE > 0){
					return "CONSUMING_ENDPOINT_OUT";	
				}else{
					return "CONSUMING_ENDPOINT_IN";
				}
			}else{
				if(statistic.sendDONE > 0){
					return "PROVIDING_ENDPOINT_OUT";	
				}else{
					return "PROVIDING_ENDPOINT_IN";
				}
			}	
		}
}

var DistributionStateDataExtractor = {
		
	extract : function(statistics){
		
		var type = Qualifier.qualify(statistics);
		var extractor = null;
		switch(type){
			case "CONSUMING_ENDPOINT_OUT" :
				extractor = ConsumingOutDataExtractor;
				break;
			case "CONSUMING_ENDPOINT_IN" :
				extractor = ConsumingInDataExtractor;
				break;
			case "PROVIDING_ENDPOINT_IN" :
				extractor = ProvidingInDataExtractor;
				break;
			case "PROVIDING_ENDPOINT_OUT" :
				extractor = ProvidingOutDataExtractor;
				break;
		}
		var dataExtraction =  extractor.extract([statistics[0], statistics[statistics.length -1]]);	
		
		return[
	            { name: 'Success', y: dataExtraction[1][0][1], color: '#acd479' },
	            { name: 'Fault',   y: dataExtraction[2][0][1], color: '#fdc04d' },
	            { name: 'Error',   y: dataExtraction[3][0][1], color: '#fdbd43' }
	        ];
	}
}

/**
 * Extract statistic information to provide displayable datas
 */
var TimelineDataExtractor = {
		
	extract : function(statistics){
		
		var type = Qualifier.qualify(statistics);
		var extractor = null;
		switch(type){
			case "CONSUMING_ENDPOINT_OUT" :
				extractor = ConsumingOutDataExtractor;
				break;
			case "CONSUMING_ENDPOINT_IN" :
				extractor = ConsumingInDataExtractor;
				break;
			case "PROVIDING_ENDPOINT_IN" :
				extractor = ProvidingInDataExtractor;
				break;
			case "PROVIDING_ENDPOINT_OUT" :
				extractor = ProvidingOutDataExtractor;
				break;
		}
		return extractor.extract(statistics);
		
	}
}



/**
 * CONSUMING_ENDPOINT_OUT
 */
var ConsumingOutDataExtractor = {
	
	//Initialize the data structure
	_init : function(){
		 return [new Array(),new Array(),new Array(), new Array()];
	},
	
	extract : function(datas){
		var res = this._init();
		var previous = null;
		$.each( datas, function( key, value ) {
			if(key > 0){
				res[0].push([value.date, value.sendDONE     - previous.sendDONE]);
				res[1].push([value.date, value.receiveReply - previous.receiveReply]);
			    res[2].push([value.date, value.receiveFault - previous.receiveFault]);
			    res[3].push([value.date, value.receiveERROR - previous.receiveERROR]);
			}
			previous = value;
		});
		return res;
	}
}

/**
 * CONSUMING_ENDPOINT_IN
 */
var ConsumingInDataExtractor = {
	
	//Initialize the data structure
	_init : function(){
		 return [new Array(),new Array(),new Array(), new Array()];
	},
	
	extract : function(datas){
		var res = this._init();
		var previous = null;
		$.each( datas, function( key, value ) {
			if(key > 0){
				res[0].push([value.date,value.receiveDONE  - previous.receiveDONE]);
				res[1].push([value.date,value.receiveReply - previous.receiveReply]);
			    res[2].push([value.date,value.receiveFault - previous.receiveFault]);
			    res[3].push([value.date,value.receiveERROR - previous.receiveERROR]);
			}
			previous = value;
		});
		return res;
	}
}


/**
 * PROVIDING_ENDPOINT_IN
 */
var ProvidingInDataExtractor = {
	
	//Initialize the data structure
	_init : function(){
		 return [new Array(),new Array(),new Array(), new Array()];
	},
	
	extract : function(datas){
		var res = this._init();
		var previous = null;
		$.each( datas, function( key, value ) {
			if(key > 0){
				res[0].push([value.date, value.receiveRequest - previous.receiveRequest]);
				res[1].push([value.date, value.sendReply      - previous.sendReply]);
			    res[2].push([value.date, value.sendFault      - previous.sendFault]);
			    res[3].push([value.date, value.sendERROR      - previous.sendERROR]);
			}
			previous = value;
		});
		return res;
	}
}


/**
 * PROVIDING_ENDPOINT_OUT
 */
var ProvidingOutDataExtractor = {
	
	//Initialize the data structure
	_init : function(){
		 return [new Array(),new Array(),new Array(), new Array()];
	},
	
	extract : function(datas){
		var res = this._init();
		var previous = null;
		$.each( datas, function( key, value ) {
			if(key > 0){
				res[0].push([value.date, value.sendDONE  - previous.sendDONE]);
				res[1].push([value.date, value.sendReply - previous.sendReply]);
			    res[2].push([value.date, value.sendFault - previous.sendFault]);
			    res[3].push([value.date, value.sendERROR - previous.sendERROR]);
			}
			previous = value;
		});
		return res;
	}
}
