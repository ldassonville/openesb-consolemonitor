package net.openesb.consolemonitor.persistence.entity.criteria;

import java.io.Serializable;

public class InstanceCriteria implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String instanceGroupName;
	private String hostName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstanceGroupName() {
		return instanceGroupName;
	}

	public void setInstanceGroupName(String instanceGroupName) {
		this.instanceGroupName = instanceGroupName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
}
