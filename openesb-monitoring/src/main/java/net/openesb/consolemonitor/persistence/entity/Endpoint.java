package net.openesb.consolemonitor.persistence.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;
import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;


/**
 * @author loic.dassonville
 * Entity for {@link Endpoint}
 */
@Entity
public class Endpoint implements net.openesb.consolemonitor.persistence.entity.Entity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@Column(name="name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="component_id", nullable=true, updatable=true)
	private Component component;
	
	@ManyToOne
	@JoinColumn(name="instance_id", nullable=false, updatable=false)
	private Instance instance;
	
	@OneToMany(mappedBy="endpoint")
	private List<EndpointStatistic> statistics;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public List<EndpointStatistic> getStatistics() {
		return statistics;
	}

	public void setStatistics(List<EndpointStatistic> statistics) {
		this.statistics = statistics;
	}

	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		this.instance = instance;
	}
}
