package net.openesb.consolemonitor.persistence.entity.statistic;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.openesb.consolemonitor.persistence.entity.Component;
import net.openesb.consolemonitor.persistence.entity.Endpoint;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;


/**
 * @author loic.dassonville
 * Entity for endpoint statistics
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class EndpointStatistic implements net.openesb.consolemonitor.persistence.entity.Entity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
    
	@ManyToOne
	@JoinColumn(name="endpoint_id", nullable=false, updatable=false)
	private Endpoint endpoint;
	
	@ManyToOne 
	@JoinColumn(name="instance_id", nullable=false, updatable=false)
	private Instance instance;
	
	@ManyToOne
	@JoinColumn(name="component_id", nullable=true, updatable=true)
	private Component component;
	
	@Column(name="activation_timestamp")
	private Date activationTimestamp;
	
	@Column(name="active_exchanges")
	private Long activeExchanges;
	
	@Column(name="last_done_time")
	private Date lastDONETime;
	
	@Column(name="last_error_time")
	private Date lastERRORTime;
	
	@Column(name="last_fault_time")
	private Date lastFaultTime;
	
	@Column(name="owning_channel")
	private String owningChannel;
	
	@Column(name="receive_done")
	private Long receiveDONE;
	
	@Column(name="receive_error")
	private Long receiveERROR;
	
	@Column(name="receive_fault")
	private Long receiveFault;
	
	@Column(name="receive_reply")
	private Long receiveReply;
	
	@Column(name="receive_request")
	private Long receiveRequest;
	
	@Column(name="send_done")
	private Long sendDONE;
	
	@Column(name="send_error")
	private Long sendERROR;
	
	@Column(name="send_fault")
	private Long sendFault;
	
	@Column(name="send_reply")
	private Long sendReply;
	
	@Column(name="send_request")
	private Long sendRequest;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
		
	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance node) {
		this.instance = node;
	}

	public Date getActivationTimestamp() {
		return activationTimestamp;
	}

	public void setActivationTimestamp(Date activationTimestamp) {
		this.activationTimestamp = activationTimestamp;
	}

	public Long getActiveExchanges() {
		return activeExchanges;
	}

	public void setActiveExchanges(Long activeExchanges) {
		this.activeExchanges = activeExchanges;
	}

	public Date getLastDONETime() {
		return lastDONETime;
	}

	public void setLastDONETime(Date lastDONETime) {
		this.lastDONETime = lastDONETime;
	}

	public Date getLastERRORTime() {
		return lastERRORTime;
	}

	public void setLastERRORTime(Date lastERRORTime) {
		this.lastERRORTime = lastERRORTime;
	}

	public Date getLastFaultTime() {
		return lastFaultTime;
	}

	public void setLastFaultTime(Date lastFaultTime) {
		this.lastFaultTime = lastFaultTime;
	}

	public String getOwningChannel() {
		return owningChannel;
	}

	public void setOwningChannel(String owningChannel) {
		this.owningChannel = owningChannel;
	}

	public Long getReceiveDONE() {
		return receiveDONE;
	}

	public void setReceiveDONE(Long receiveDONE) {
		this.receiveDONE = receiveDONE;
	}

	public Long getReceiveERROR() {
		return receiveERROR;
	}

	public void setReceiveERROR(Long receiveERROR) {
		this.receiveERROR = receiveERROR;
	}

	public Long getReceiveFault() {
		return receiveFault;
	}

	public void setReceiveFault(Long receiveFault) {
		this.receiveFault = receiveFault;
	}

	public Long getReceiveReply() {
		return receiveReply;
	}

	public void setReceiveReply(Long receiveReply) {
		this.receiveReply = receiveReply;
	}

	public Long getReceiveRequest() {
		return receiveRequest;
	}

	public void setReceiveRequest(Long receiveRequest) {
		this.receiveRequest = receiveRequest;
	}

	public Long getSendDONE() {
		return sendDONE;
	}

	public void setSendDONE(Long sendDONE) {
		this.sendDONE = sendDONE;
	}

	public Long getSendERROR() {
		return sendERROR;
	}

	public void setSendERROR(Long sendERROR) {
		this.sendERROR = sendERROR;
	}

	public Long getSendFault() {
		return sendFault;
	}

	public void setSendFault(Long sendFault) {
		this.sendFault = sendFault;
	}

	public Long getSendReply() {
		return sendReply;
	}

	public void setSendReply(Long sendReply) {
		this.sendReply = sendReply;
	}

	public Long getSendRequest() {
		return sendRequest;
	}

	public void setSendRequest(Long sendRequest) {
		this.sendRequest = sendRequest;
	}
  
}
