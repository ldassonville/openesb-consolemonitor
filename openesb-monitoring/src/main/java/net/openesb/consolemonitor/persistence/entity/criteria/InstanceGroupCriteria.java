package net.openesb.consolemonitor.persistence.entity.criteria;

import java.io.Serializable;

public class InstanceGroupCriteria implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
