package net.openesb.consolemonitor.persistence.entity.infrastructure;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.openesb.consolemonitor.persistence.entity.Endpoint;

@Entity
@Table(uniqueConstraints={
	@UniqueConstraint(columnNames="name"),
	//@UniqueConstraint(columnNames= {"host_id", "instanceGroup_id"})
})
@NamedQueries(value={
		@NamedQuery(name="FIND_INSTANCE_BY_NAME", query="from Instance where name=:name")
})
public class Instance implements net.openesb.consolemonitor.persistence.entity.Entity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column
	private Integer jmxPort;
	
	@ManyToOne
	@JoinColumn(name="instanceGroup_id", nullable=false, updatable=false)
	private InstanceGroup instanceGroup;
	
	@ManyToOne
	@JoinColumn(name="host_id", nullable=false, updatable=false)
	private Host host;
	
	@OneToMany(mappedBy="instance")
	private List<Endpoint> endpoints;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getJmxPort() {
		return jmxPort;
	}

	public void setJmxPort(Integer jmxPort) {
		this.jmxPort = jmxPort;
	}

	public InstanceGroup getInstanceGroup() {
		return instanceGroup;
	}

	public void setInstanceGroup(InstanceGroup instanceGroup) {
		this.instanceGroup = instanceGroup;
	}

	public Host getHost() {
		return host;
	}

	public void setHost(Host host) {
		this.host = host;
	}

	public List<Endpoint> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<Endpoint> endpoints) {
		this.endpoints = endpoints;
	}

}
