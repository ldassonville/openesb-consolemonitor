package net.openesb.consolemonitor.persistence.dao;


import net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author loic.dassonville
 */
public interface ClusterRepository extends JpaRepository<InstanceGroup, Long> {

}
