package net.openesb.consolemonitor.persistence.entity.criteria;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;

public class StatisticCriteria implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String endpoint;
	private List<Instance> nodes;
	private Date beginPeriode;
	private Date endPeriode;
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public List<Instance> getNodes() {
		return nodes;
	}
	public void setNodes(List<Instance> nodes) {
		this.nodes = nodes;
	}
	public Date getBeginPeriode() {
		return beginPeriode;
	}
	public void setBeginPeriode(Date beginPeriode) {
		this.beginPeriode = beginPeriode;
	}
	public Date getEndPeriode() {
		return endPeriode;
	}
	public void setEndPeriode(Date endPeriode) {
		this.endPeriode = endPeriode;
	}

}
