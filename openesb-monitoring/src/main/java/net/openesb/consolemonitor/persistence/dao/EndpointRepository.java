package net.openesb.consolemonitor.persistence.dao;


import net.openesb.consolemonitor.persistence.entity.Endpoint;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author loic.dassonville
 */
public interface EndpointRepository extends JpaRepository<Endpoint, Long> {

}
