package net.openesb.consolemonitor.persistence.dao;


import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author loic.dassonville
 */
public interface StatisticRepository extends JpaRepository<EndpointStatistic, Long> {

}
