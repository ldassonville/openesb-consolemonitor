package net.openesb.consolemonitor.persistence.dao;


import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author loic.dassonville
 */
public interface NodeRepository extends JpaRepository<Instance, Long> {

}
