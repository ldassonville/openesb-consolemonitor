package net.openesb.consolemonitor.persistence.entity;

import java.io.Serializable;

public interface Entity<T> extends Serializable{

	public T getId();
}
