package net.openesb.consolemonitor.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.dao.StatisticRepository;
import net.openesb.consolemonitor.persistence.entity.criteria.EndpointCriteria;
import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;
import net.openesb.consolemonitor.service.itf.StatisticService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 
 * Statistic service.
 * 
 * @see StatisticService 
 * @author loic Dassonville
 *
 */
@Service
@Transactional
public class StatisticServiceImpl implements StatisticService   {

    private static final Logger logger = LoggerFactory.getLogger(StatisticServiceImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private StatisticRepository statisticRepository;
    
	@Override
	public List<EndpointStatistic> findAll() throws ServiceException {
		return statisticRepository.findAll();
	}

	public EndpointStatistic save(EndpointStatistic endpointStatistic)throws ServiceException{	
		return statisticRepository.save(endpointStatistic);
	}
    
	public List<EndpointStatistic> findByCriteria(EndpointCriteria criteria){
		return new ArrayList<>();
		
	}
}