package net.openesb.consolemonitor.service.itf;

import java.util.List;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.entity.criteria.InstanceCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;

/**
 * 
 * Cluster node service.
 * 
 * @author loic.dassonville
 *
 */
public interface InstanceService {

	/**
	 * Return an instance by Id
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	public Instance getById(Long id) throws ServiceException;
	
	
	public Instance getByName(String name) throws ServiceException;
	
	public List<Instance> findAll() throws ServiceException;
	
	public Instance save(Instance node) throws ServiceException;
	
	public List<Instance> findByCriteria(InstanceCriteria criteria) throws ServiceException;
	
}