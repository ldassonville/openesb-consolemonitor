package net.openesb.consolemonitor.service.itf;

import java.util.List;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.entity.criteria.EndpointCriteria;
import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;

/**
 * 
 * Statistic service.
 * 
 * @author loic.dassonville
 *
 */
public interface StatisticService {

	/**
	 * return all statistics.
	 * 
	 * @throws ServiceException
	 */
	public List<EndpointStatistic> findAll() throws ServiceException;
	
	public EndpointStatistic save(EndpointStatistic endpointStatistic) throws ServiceException;
	
	public List<EndpointStatistic> findByCriteria(EndpointCriteria criteria) throws ServiceException;

}