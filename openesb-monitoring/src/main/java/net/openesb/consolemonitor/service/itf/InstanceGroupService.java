package net.openesb.consolemonitor.service.itf;

import java.util.List;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.entity.criteria.InstanceGroupCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup;

/**
 * 
 * Cluster service.
 * 
 * @author loic.dassonville
 *
 */
public interface InstanceGroupService {
	
	public InstanceGroup getById(Long id) throws ServiceException;
	
	public List<InstanceGroup> findAll() throws Exception;
	
	public List<InstanceGroup> findByCriteria(InstanceGroupCriteria criteria) throws Exception;
	
	public InstanceGroup save(InstanceGroup group) throws ServiceException;
	
	public void delete(InstanceGroup instanceGroup) throws ServiceException;
	
}