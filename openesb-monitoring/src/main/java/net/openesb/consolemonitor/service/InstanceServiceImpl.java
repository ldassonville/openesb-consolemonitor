package net.openesb.consolemonitor.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.dao.NodeRepository;
import net.openesb.consolemonitor.persistence.entity.criteria.InstanceCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Host;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;
import net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup;
import net.openesb.consolemonitor.service.itf.InstanceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InstanceServiceImpl implements InstanceService {
    
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private NodeRepository nodeRepository;
    
    @Override
	public Instance getById(Long id) throws ServiceException{
    	return nodeRepository.findOne(id);
    }
    
	@Override
	public Instance getByName(String name) throws ServiceException {
 		TypedQuery<Instance> query = em.createNamedQuery("FIND_INSTANCE_BY_NAME", Instance.class);
 		query.setParameter("name", name);
		return query.getSingleResult();
	}
	
	@Override
	public List<Instance> findAll() throws ServiceException {
		return nodeRepository.findAll();
	}
	
	@Override
	public Instance save(Instance cluster) throws ServiceException {
		return nodeRepository.save(cluster);
	}
	
	@Override
	public List<Instance> findByCriteria(InstanceCriteria criteria) throws ServiceException {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Instance>  query = criteriaBuilder.createQuery(Instance.class);
		Root<Instance> nodeRoot = query.from(Instance.class);
			
		List<Predicate> predicates = new ArrayList<>();
		if(criteria.getName() != null){
			predicates.add(criteriaBuilder.equal(nodeRoot.get("name"), criteria.getName()));
		}
		if(criteria.getInstanceGroupName() != null){
			Join<Instance, InstanceGroup> joinCluster = nodeRoot.join("instanceGroup");
			predicates.add(criteriaBuilder.equal(joinCluster.get("name"), criteria.getInstanceGroupName()));
		}
		
		if(criteria.getHostName() != null){
			Join<Instance, Host> joinHost = nodeRoot.join("host");
			predicates.add(criteriaBuilder.equal(joinHost.get("name"), criteria.getHostName()));			
		}
		
		query.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Instance> typedQuery = em.createQuery(query);
		return typedQuery.getResultList();	
		
	}	
}
