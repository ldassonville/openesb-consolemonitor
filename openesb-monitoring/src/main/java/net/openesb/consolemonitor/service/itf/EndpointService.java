package net.openesb.consolemonitor.service.itf;

import java.util.List;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.entity.Endpoint;
import net.openesb.consolemonitor.persistence.entity.criteria.EndpointCriteria;

/**
 * 
 * Statistic service.
 * 
 * @author loic.dassonville
 *
 */
public interface EndpointService {

	public Endpoint getById(Long id) throws ServiceException;
	
	/**
	 * return endpoints.
	 * 
	 * @throws ServiceException
	 */
	public List<Endpoint> findAll() throws ServiceException;
	
	public List<Endpoint> findByCriteria(EndpointCriteria criteria) throws ServiceException;
	
	public Endpoint save(Endpoint endpoint)throws ServiceException;

}