package net.openesb.consolemonitor.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.dao.ClusterRepository;
import net.openesb.consolemonitor.persistence.entity.criteria.InstanceGroupCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup;
import net.openesb.consolemonitor.service.itf.InstanceGroupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GroupeInstanceServiceImpl implements InstanceGroupService {
    
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ClusterRepository clusterRepository;
	
	@Override
	public InstanceGroup getById(Long id) throws ServiceException {
		return clusterRepository.findOne(id);
	}
	
	@Override
	public List<InstanceGroup> findAll() throws ServiceException {
		return clusterRepository.findAll();
	}
	   
	@Override
	public List<InstanceGroup> findByCriteria(InstanceGroupCriteria criteria) throws Exception {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<InstanceGroup>  query = criteriaBuilder.createQuery(InstanceGroup.class);
		Root<InstanceGroup> rootCluster = query.from(InstanceGroup.class);
		if(criteria.getName() != null){
			query.where(criteriaBuilder.equal(rootCluster.get("name"), criteria.getName()));
		}
		TypedQuery<InstanceGroup> typedQuery = em.createQuery(query);
		return typedQuery.getResultList();
	}

	@Override
	public InstanceGroup save(InstanceGroup group) throws ServiceException {
		return clusterRepository.save(group);
	}
	
	@Override
	public void delete(InstanceGroup instanceGroup) throws ServiceException {
		clusterRepository.delete(instanceGroup);
	}

}
