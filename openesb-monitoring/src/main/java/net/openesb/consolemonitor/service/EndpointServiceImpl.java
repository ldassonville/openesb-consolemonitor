package net.openesb.consolemonitor.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.dao.EndpointRepository;
import net.openesb.consolemonitor.persistence.entity.Endpoint;
import net.openesb.consolemonitor.persistence.entity.criteria.EndpointCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;
import net.openesb.consolemonitor.service.itf.EndpointService;
import net.openesb.consolemonitor.service.itf.StatisticService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 
 * Endpoint service.
 * 
 * @see StatisticService 
 * @author loic dassonville
 *
 */
@Service
public class EndpointServiceImpl implements EndpointService   {

    private static final Logger logger = LoggerFactory.getLogger(EndpointServiceImpl.class);
    
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private EndpointRepository endpointRepository;
    
	@Override
	public List<Endpoint> findAll() throws ServiceException {
		return endpointRepository.findAll();
	}
	
	public Endpoint save(Endpoint endpoint)throws ServiceException{
		logger.debug("Save endpointStatistic");
		return endpointRepository.save(endpoint);
	}
    
	public List<Endpoint> findByCriteria(EndpointCriteria criteria){
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Endpoint>  query = criteriaBuilder.createQuery(Endpoint.class);
		Root<Endpoint> endpointRoot = query.from(Endpoint.class);
		List<Predicate> predicates = new ArrayList<>();
		if(criteria.getName() != null){
			predicates.add(criteriaBuilder.equal(endpointRoot.get("name"), criteria.getName()));
		}
		if(criteria.getInstanceName() != null){
			Join<Instance, Instance> joinInstance = endpointRoot.join("instance");
			predicates.add(criteriaBuilder.equal(joinInstance.get("name"), criteria.getInstanceName()));
		}
		query.where(predicates.toArray(new Predicate[predicates.size()]));
		
		TypedQuery<Endpoint> typedQuery = em.createQuery(query);
		return typedQuery.getResultList();	
	}

	@Override
	public Endpoint getById(Long id) throws ServiceException {
		return endpointRepository.findOne(id);
	}
}