package net.openesb.consolemonitor.service.itf;

import java.util.List;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.entity.criteria.HostCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Host;

/**
 * 
 * Cluster service.
 * 
 * @author loic.dassonville
 *
 */
public interface HostService {
	
	public Host getById(Long id) throws Exception;

	public List<Host> findByCriteria(HostCriteria criteria) throws Exception;
	
	public List<Host> findAll() throws Exception;
	
	public Host save(Host host) throws ServiceException;
	
}