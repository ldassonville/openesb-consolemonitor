package net.openesb.consolemonitor.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import net.openesb.consolemonitor.exception.ServiceException;
import net.openesb.consolemonitor.persistence.dao.HostRepository;
import net.openesb.consolemonitor.persistence.entity.criteria.HostCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Host;
import net.openesb.consolemonitor.service.itf.HostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class HostServiceImpl implements HostService {
    
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private HostRepository hostRepository;
    
	@Override
	public List<Host> findByCriteria(HostCriteria criteria) throws Exception {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Host>  query = criteriaBuilder.createQuery(Host.class);
		Root<Host> hostRoot = query.from(Host.class);
		if(criteria.getName() != null){
			query.where(criteriaBuilder.equal(hostRoot.get("name"), criteria.getName()));
		}
		TypedQuery<Host> typedQuery = em.createQuery(query);
		return typedQuery.getResultList();	
	}
	
	@Override
	public List<Host> findAll() throws ServiceException {
		return hostRepository.findAll();
	}
	
	public Host save(Host host) throws ServiceException {
		return hostRepository.save(host);
	}

	@Override
	public Host getById(Long id) throws Exception {
		return hostRepository.findOne(id);
	}

}
