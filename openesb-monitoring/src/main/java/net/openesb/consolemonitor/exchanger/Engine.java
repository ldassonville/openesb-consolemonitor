 package net.openesb.consolemonitor.exchanger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import net.openesb.consolemonitor.exchanger.consumer.Consumer;
import net.openesb.consolemonitor.exchanger.model.EndpointStatistic;
import net.openesb.consolemonitor.exchanger.model.Host;
import net.openesb.consolemonitor.exchanger.model.Instance;
import net.openesb.consolemonitor.exchanger.model.InstanceGroup;
import net.openesb.consolemonitor.exchanger.provider.Collector;
import net.openesb.consolemonitor.exchanger.provider.CollectorContext;
import net.openesb.consolemonitor.exchanger.provider.JMXDataCollector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Engine {

	private Logger logger = LoggerFactory.getLogger(Engine.class);
	private ApplicationContext applicationContext;
	
	public void init(){
		this.applicationContext = new  ClassPathXmlApplicationContext(
				"classpath:spring/applicationContext.xml" ,
				"classpath:spring/applicationContext-repository.xml",
				"classpath:spring/applicationContext-service.xml",
				"classpath:spring/applicationContext-collector.xml");		
	}
	
	public static void main(String[] args) throws Exception {

		Engine engine = new Engine();
		engine.init();
		engine.run();
	}
	
	public void run() throws Exception{
		BlockingQueue<EndpointStatistic> queue = new LinkedBlockingQueue<>(2048);
		
		List<Host> hosts = Arrays.asList(
				new Host("localhost")
		);
		
		InstanceGroup groupProduit = new InstanceGroup("produit");
		InstanceGroup groupClient =  new InstanceGroup("client");
		InstanceGroup groupVAD =     new InstanceGroup("VAD");
		
		launchCollector(queue, hosts, groupProduit, 38686);
		launchCollector(queue, hosts, groupClient, 38687);
		launchCollector(queue, hosts, groupVAD, 38688);

		Consumer<EndpointStatistic> c1 = applicationContext.getBean(Consumer.class);
		c1.setQueue(queue);
		new Thread(c1).start();	
		
		Consumer<EndpointStatistic> c2 = applicationContext.getBean(Consumer.class);
		c2.setQueue(queue);
		new Thread(c2).start();	
		
		Consumer<EndpointStatistic> c4 = applicationContext.getBean(Consumer.class);
		c4.setQueue(queue);
		new Thread(c4).start();	
		
		Consumer<EndpointStatistic> c5 = applicationContext.getBean(Consumer.class);
		c5.setQueue(queue);
		new Thread(c5).start();	
	}
	
	
	private void launchCollector(BlockingQueue<EndpointStatistic> queue, List<Host> hosts, InstanceGroup instangeGroup, Integer port){
		
		for (Host host : hosts) {
			try{
				Instance node = new Instance(host, instangeGroup, port);
				new Thread(getCollector(queue, node)).start();
			}catch(Exception e){
				logger.error("Error lauching collecor", e);
			}
		}
		
	}

	private Collector<EndpointStatistic> getCollector(BlockingQueue<EndpointStatistic> queue, Instance instance){
		
		CollectorContext context = new CollectorContext();
		context.setLogin("admin");
		context.setPassword("adminadmin");
		context.setInstance(instance);
		context.setUrl("service:jmx:rmi:///jndi/rmi://"+instance.getHost().getName()+":"+instance.getPort()+"/jmxrmi");
		return new JMXDataCollector(queue, context);
	}
}
