package net.openesb.consolemonitor.exchanger.provider;

import java.io.Serializable;

import net.openesb.consolemonitor.exchanger.model.Instance;

public class CollectorContext implements Serializable{

	private static final long serialVersionUID = 1L;
	private String url;
	private Instance instance;
	private String login;
	private String password;

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Instance getInstance() {
		return instance;
	}
	public void setInstance(Instance instance) {
		this.instance = instance;
	}
	
}

