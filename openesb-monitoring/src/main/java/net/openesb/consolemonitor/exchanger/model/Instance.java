package net.openesb.consolemonitor.exchanger.model;

import java.io.Serializable;

public class Instance implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer port;
	private String name;
	private Host host;
	private InstanceGroup instanceGroup;

	public Instance() {}
	
	public Instance(Host host, InstanceGroup instanceGroup, Integer port) {
		this.name = host.getName() +"-"+ instanceGroup.getName();
		this.host = host;
		this.instanceGroup = instanceGroup;
		this.port = port;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Host getHost() {
		return host;
	}

	public void setHost(Host host) {
		this.host = host;
	}

	public InstanceGroup getInstanceGroup() {
		return instanceGroup;
	}

	public void setInstanceGroup(InstanceGroup instanceGroup) {
		this.instanceGroup = instanceGroup;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
}
