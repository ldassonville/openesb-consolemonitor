package net.openesb.consolemonitor.exchanger.model;

import java.io.Serializable;

public class InstanceGroup implements Serializable{

	private static final long serialVersionUID = 1L;
	private String name;

	public InstanceGroup(){}
	
	public InstanceGroup(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
