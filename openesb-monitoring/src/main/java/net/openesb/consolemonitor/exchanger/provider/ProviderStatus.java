package net.openesb.consolemonitor.exchanger.provider;

public enum ProviderStatus {

	RUNNING,
	STOPPED,
	INTERRUPTED;
}
