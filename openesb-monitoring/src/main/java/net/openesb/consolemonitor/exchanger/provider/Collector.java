package net.openesb.consolemonitor.exchanger.provider;

import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Collector<T> implements Runnable {

	private Logger logger = LoggerFactory.getLogger(Collector.class);
	private ProviderStatus status;
	private CollectorContext context;
	private Queue<T> queue;
	
	public Collector(Queue<T> queue, CollectorContext context){
		this.queue = queue;
		this.context = context;
	}

	public abstract void run();
	
	protected void push(T elt){
		logger.debug("Push element");
		this.queue.add(elt);
	}

	public ProviderStatus getStatus() {
		return status;
	}

	public void setStatus(ProviderStatus status) {
		this.status = status;
	}

	public Queue<T> getQueue() {
		return queue;
	}

	public CollectorContext getContext() {
		return context;
	}

	public void setContext(CollectorContext context) {
		this.context = context;
	}
}
