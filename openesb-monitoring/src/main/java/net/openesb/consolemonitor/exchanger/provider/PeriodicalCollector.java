package net.openesb.consolemonitor.exchanger.provider;

import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class PeriodicalCollector<T> extends Collector<T>{

	private Logger logger = LoggerFactory.getLogger(PeriodicalCollector.class);
	
	private ScheduledFuture<?> collectorHandle;
	
	public PeriodicalCollector(Queue<T> queue, CollectorContext context){
		super(queue, context);
	}

	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	protected abstract void collecte() throws Exception;
	
	public void run() {

		final Runnable collectorBinding = new Runnable() {
			public void run() {
				
				try{
					collecte();
				}catch(Exception e){
					logger.error("PeriodicalCollector", e);
				}
			}
		};
		this.collectorHandle = scheduler.scheduleAtFixedRate(collectorBinding, 0, 15, TimeUnit.MINUTES);	
	}
	
	public void disable(){
		collectorHandle.cancel(true);
	}
}
