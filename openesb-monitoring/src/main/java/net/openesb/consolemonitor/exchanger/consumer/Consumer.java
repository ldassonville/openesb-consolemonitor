package net.openesb.consolemonitor.exchanger.consumer;

import java.util.concurrent.BlockingQueue;

public interface Consumer<T> extends Runnable{
	
	/**
	 * Set the queue to consume.
	 *  
	 * @param queue
	 */
	public void setQueue(BlockingQueue<T> queue);
	
	/**
	 * Process element queue
	 * 
	 * @param elt
	 * @throws Exception
	 */
	public void process(T elt) throws Exception; 

}
