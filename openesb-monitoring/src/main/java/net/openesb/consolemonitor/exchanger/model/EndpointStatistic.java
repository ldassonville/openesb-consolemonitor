package net.openesb.consolemonitor.exchanger.model;

import java.io.Serializable;
import java.util.Date;


/**
 * @author loic.dassonville
 * Endpoint statistics line
 */
public class EndpointStatistic implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date date;

	private String endpoint;

	private Instance instance;

	private Date activationTimestamp;

	private Long activeExchanges;

	private Date lastDONETime;

	private Date lastERRORTime;

	private Date lastFaultTime;

	private String owningChannel;

	private Long receiveDONE;

	private Long receiveERROR;

	private Long receiveFault;

	private Long receiveReply;

	private Long receiveRequest;

	private Long sendDONE;

	private Long sendERROR;

	private Long sendFault;

	private Long sendReply;

	private Long sendRequest;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	public Date getActivationTimestamp() {
		return activationTimestamp;
	}

	public void setActivationTimestamp(Date activationTimestamp) {
		this.activationTimestamp = activationTimestamp;
	}

	public Long getActiveExchanges() {
		return activeExchanges;
	}

	public void setActiveExchanges(Long activeExchanges) {
		this.activeExchanges = activeExchanges;
	}

	public Date getLastDONETime() {
		return lastDONETime;
	}

	public void setLastDONETime(Date lastDONETime) {
		this.lastDONETime = lastDONETime;
	}

	public Date getLastERRORTime() {
		return lastERRORTime;
	}

	public void setLastERRORTime(Date lastERRORTime) {
		this.lastERRORTime = lastERRORTime;
	}

	public Date getLastFaultTime() {
		return lastFaultTime;
	}

	public void setLastFaultTime(Date lastFaultTime) {
		this.lastFaultTime = lastFaultTime;
	}

	public String getOwningChannel() {
		return owningChannel;
	}

	public void setOwningChannel(String owningChannel) {
		this.owningChannel = owningChannel;
	}

	public Long getReceiveDONE() {
		return receiveDONE;
	}

	public void setReceiveDONE(Long receiveDONE) {
		this.receiveDONE = receiveDONE;
	}

	public Long getReceiveERROR() {
		return receiveERROR;
	}

	public void setReceiveERROR(Long receiveERROR) {
		this.receiveERROR = receiveERROR;
	}

	public Long getReceiveFault() {
		return receiveFault;
	}

	public void setReceiveFault(Long receiveFault) {
		this.receiveFault = receiveFault;
	}

	public Long getReceiveReply() {
		return receiveReply;
	}

	public void setReceiveReply(Long receiveReply) {
		this.receiveReply = receiveReply;
	}

	public Long getReceiveRequest() {
		return receiveRequest;
	}

	public void setReceiveRequest(Long receiveRequest) {
		this.receiveRequest = receiveRequest;
	}

	public Long getSendDONE() {
		return sendDONE;
	}

	public void setSendDONE(Long sendDONE) {
		this.sendDONE = sendDONE;
	}

	public Long getSendERROR() {
		return sendERROR;
	}

	public void setSendERROR(Long sendERROR) {
		this.sendERROR = sendERROR;
	}

	public Long getSendFault() {
		return sendFault;
	}

	public void setSendFault(Long sendFault) {
		this.sendFault = sendFault;
	}

	public Long getSendReply() {
		return sendReply;
	}

	public void setSendReply(Long sendReply) {
		this.sendReply = sendReply;
	}

	public Long getSendRequest() {
		return sendRequest;
	}

	public void setSendRequest(Long sendRequest) {
		this.sendRequest = sendRequest;
	}
}
