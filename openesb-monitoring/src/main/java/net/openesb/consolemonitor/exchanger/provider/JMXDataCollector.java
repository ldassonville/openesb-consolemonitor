package net.openesb.consolemonitor.exchanger.provider;

import java.util.HashMap;
import java.util.Queue;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import net.openesb.consolemonitor.exchanger.model.EndpointStatistic;
import net.openesb.consolemonitor.exchanger.model.Instance;
import net.openesb.consolemonitor.exchanger.provider.mapper.StatisticLineParser;


public class JMXDataCollector extends PeriodicalCollector<EndpointStatistic> {

	private String url;
	private String[] credentials;
	
	public JMXDataCollector(Queue<EndpointStatistic> queue, CollectorContext context) {
		super(queue, context);
		this.url = context.getUrl();
		credentials = new String[]{context.getLogin(), context.getPassword()};
	}

	@Override
	protected void collecte() throws Exception{

		MBeanServerConnection msc = getConnection();
		Instance instance = getContext().getInstance();
		ObjectName messageService = new ObjectName("com.sun.jbi:JbiName="+instance.getInstanceGroup().getName()+"-"+instance.getHost().getName()+",ServiceName=MessageService,ControlType=Statistics,ComponentType=System");
		
		String[] activeEndpoints = (String[]) msc.getAttribute(messageService, "ActiveEndpoints");
		if(activeEndpoints != null){
			for (String activeEndpoint : activeEndpoints) {
				System.out.println(activeEndpoint);
				CompositeData compositeData = (CompositeData) msc.invoke(messageService, "getEndpointStatistics", new Object[]{activeEndpoint}, new String[]{"java.lang.String"});
				push(StatisticLineParser.parse(activeEndpoint, instance, compositeData));
			}
		}	
		
	}

	protected MBeanServerConnection getConnection() throws Exception {
		HashMap<String, String[]> environment = new HashMap<String, String[]>();
        environment.put(JMXConnector.CREDENTIALS, credentials);

        JMXServiceURL url = new JMXServiceURL(this.url);
        JMXConnector jmxc = JMXConnectorFactory.connect(url, environment);
        MBeanServerConnection msc = jmxc.getMBeanServerConnection();
        return msc;
	}
}
