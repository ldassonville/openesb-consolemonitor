package net.openesb.consolemonitor.exchanger.consumer;

public enum ConsumerStatus {

	RUNNING,
	STOPPED,
	INTERRUPTED;
}
