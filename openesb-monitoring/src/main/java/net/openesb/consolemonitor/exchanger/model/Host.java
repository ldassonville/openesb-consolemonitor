package net.openesb.consolemonitor.exchanger.model;

import java.io.Serializable;

public class Host implements Serializable {

	private static final long serialVersionUID = -7405358017457525841L;
	private String name;

	public Host() {}
	
	public Host(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
