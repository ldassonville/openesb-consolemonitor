package net.openesb.consolemonitor.exchanger.provider.mapper;

import java.util.Date;

import javax.management.openmbean.CompositeData;

import net.openesb.consolemonitor.exchanger.model.EndpointStatistic;
import net.openesb.consolemonitor.exchanger.model.Instance;


public class StatisticLineParser {

	
	public static EndpointStatistic parse(String endpoint, Instance instance, CompositeData compositeData){
		EndpointStatistic res = new EndpointStatistic();
		
		res.setEndpoint(endpoint);
		res.setDate(new Date());
		res.setInstance(instance);

		if(compositeData.containsKey("ActivationTimestamp") && (Long)compositeData.get("ActivationTimestamp") != 0)res.setActivationTimestamp(new Date((Long)compositeData.get("ActivationTimestamp")));
		if(compositeData.containsKey("ActiveExchanges"))res.setActiveExchanges((Long)compositeData.get("ActiveExchanges"));
		if(compositeData.containsKey("LastDONETime") && (Long)compositeData.get("LastDONETime") != 0)res.setLastDONETime(new Date((Long)compositeData.get("LastDONETime")));
		if(compositeData.containsKey("LastERRORTime") && (Long)compositeData.get("LastERRORTime") != 0)res.setLastERRORTime(new Date((Long)compositeData.get("LastERRORTime")));
		if(compositeData.containsKey("LastFaultTime") && (Long)compositeData.get("LastFaultTime") != 0)res.setLastFaultTime(new Date((Long)compositeData.get("LastFaultTime")));
		if(compositeData.containsKey("OwningChannel"))res.setOwningChannel((String)compositeData.get("OwningChannel"));
		if(compositeData.containsKey("ReceiveDONE"))res.setReceiveDONE((Long)compositeData.get("ReceiveDONE"));
		if(compositeData.containsKey("ReceiveERROR"))res.setReceiveERROR((Long)compositeData.get("ReceiveERROR"));
		if(compositeData.containsKey("ReceiveFault"))res.setReceiveFault((Long)compositeData.get("ReceiveFault"));
		
		if(compositeData.containsKey("ReceiveReply"))res.setReceiveReply((Long)compositeData.get("ReceiveReply"));
		if(compositeData.containsKey("ReceiveRequest"))res.setReceiveRequest((Long)compositeData.get("ReceiveRequest"));
		if(compositeData.containsKey("SendDONE"))res.setSendDONE((Long)compositeData.get("SendDONE"));
		if(compositeData.containsKey("SendERROR"))res.setSendERROR((Long)compositeData.get("SendERROR"));
		if(compositeData.containsKey("SendFault"))res.setSendFault((Long)compositeData.get("SendFault"));
		if(compositeData.containsKey("SendReply"))res.setSendReply((Long)compositeData.get("SendReply"));
		if(compositeData.containsKey("SendRequest"))res.setSendRequest((Long)compositeData.get("SendRequest"));
		
		return res;
	}
}
