package net.openesb.consolemonitor.exchanger.consumer;

import java.util.List;

import net.openesb.consolemonitor.exchanger.model.EndpointStatistic;
import net.openesb.consolemonitor.exchanger.model.Instance;
import net.openesb.consolemonitor.persistence.entity.Endpoint;
import net.openesb.consolemonitor.persistence.entity.criteria.EndpointCriteria;
import net.openesb.consolemonitor.persistence.entity.criteria.HostCriteria;
import net.openesb.consolemonitor.persistence.entity.criteria.InstanceCriteria;
import net.openesb.consolemonitor.persistence.entity.criteria.InstanceGroupCriteria;
import net.openesb.consolemonitor.service.itf.EndpointService;
import net.openesb.consolemonitor.service.itf.HostService;
import net.openesb.consolemonitor.service.itf.InstanceGroupService;
import net.openesb.consolemonitor.service.itf.InstanceService;
import net.openesb.consolemonitor.service.itf.StatisticService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EndpointStatisticConsumer extends AbstractConsumer<EndpointStatistic>{

	@Autowired private StatisticService statisticService;
	
	@Autowired private InstanceService instanceService;
	
	@Autowired private InstanceGroupService instanceGroupService;
	
	@Autowired private HostService hostService;	
	
	@Autowired private EndpointService endpointService;

	
	private Logger logger = LoggerFactory.getLogger(EndpointStatisticConsumer.class);

	public void process(EndpointStatistic elt) throws Exception {
		net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic endpointStatisticBo = new net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic();
		net.openesb.consolemonitor.persistence.entity.infrastructure.Instance instance = getInstance(elt.getInstance());
		endpointStatisticBo.setInstance(instance);
		endpointStatisticBo.setOwningChannel(elt.getOwningChannel());
		endpointStatisticBo.setActivationTimestamp(elt.getActivationTimestamp());
		endpointStatisticBo.setActiveExchanges(elt.getActiveExchanges());
		endpointStatisticBo.setDate(elt.getDate());
		endpointStatisticBo.setLastDONETime(elt.getLastDONETime());
		endpointStatisticBo.setLastERRORTime(elt.getLastERRORTime());
		endpointStatisticBo.setLastFaultTime(elt.getLastFaultTime());
		endpointStatisticBo.setReceiveDONE(elt.getReceiveDONE());
		endpointStatisticBo.setReceiveERROR(elt.getReceiveERROR());
		endpointStatisticBo.setReceiveFault(elt.getReceiveFault());
		endpointStatisticBo.setReceiveReply(elt.getReceiveReply());
		endpointStatisticBo.setReceiveRequest(elt.getReceiveRequest());
		endpointStatisticBo.setSendDONE(elt.getSendDONE());
		endpointStatisticBo.setSendERROR(elt.getSendERROR());
		endpointStatisticBo.setSendFault(elt.getSendFault());
		endpointStatisticBo.setSendReply(elt.getSendReply());
		endpointStatisticBo.setReceiveRequest(elt.getReceiveRequest());
		endpointStatisticBo.setEndpoint(getEndpoint(instance, elt.getEndpoint()));
		statisticService.save(endpointStatisticBo);
	}

	public Endpoint getEndpoint(net.openesb.consolemonitor.persistence.entity.infrastructure.Instance instance, String endpointName)throws Exception{
		
		EndpointCriteria criteria = new EndpointCriteria();
		criteria.setName(endpointName);
		criteria.setInstanceName(instance.getName());
		List<Endpoint> endpoints = endpointService.findByCriteria(criteria);
		Endpoint res = null;
		
		if(endpoints != null && endpoints.size() > 0){
			res = endpoints.get(0);
		}else{
			res = new Endpoint();
			res.setName(endpointName);
			res.setInstance(instance);
			res = endpointService.save(res);
		}
		return res;
	}
	
	public net.openesb.consolemonitor.persistence.entity.infrastructure.Instance getInstance(Instance instance) throws Exception{
		
		InstanceCriteria criteria = new InstanceCriteria();
		criteria.setName(instance.getName());
		List<net.openesb.consolemonitor.persistence.entity.infrastructure.Instance> instances = instanceService.findByCriteria(criteria);
		net.openesb.consolemonitor.persistence.entity.infrastructure.Instance instanceBo = null;
		if(instances == null|| instances.isEmpty() ){
			instanceBo = new net.openesb	.consolemonitor.persistence.entity.infrastructure.Instance();
			instanceBo.setName(instance.getName());
			instanceBo.setHost(getHost(instance.getHost().getName()));
			instanceBo.setInstanceGroup(getCluster(instance.getInstanceGroup().getName()));
			instanceBo = instanceService.save(instanceBo);
		}else{
			instanceBo = instances.get(0);
		}
		
		return instanceBo;
	}
	
	public net.openesb.consolemonitor.persistence.entity.infrastructure.Host getHost(String name) throws Exception{
		HostCriteria criteria = new HostCriteria();
		criteria.setName(name);
		List<net.openesb.consolemonitor.persistence.entity.infrastructure.Host> hosts = hostService.findByCriteria(criteria);
		
		if(hosts != null && hosts.size() > 0){
			return hosts.get(0);
		}
		net.openesb.consolemonitor.persistence.entity.infrastructure.Host host = new net.openesb.consolemonitor.persistence.entity.infrastructure.Host();
		host.setName(name);
		return hostService.save(host);
		
	}

	public net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup getCluster(String name)throws Exception{
		InstanceGroupCriteria criteria = new InstanceGroupCriteria();
		criteria.setName(name);
		List<net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup> clusters = instanceGroupService.findByCriteria(criteria);
		
		if(clusters != null && clusters.size() > 0){
			return clusters.get(0);
		}
		net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup instanceGroup = new net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup();
		instanceGroup.setName(name);
		return instanceGroupService.save(instanceGroup);		
	}
	
}