package net.openesb.consolemonitor.exchanger.consumer;

import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractConsumer<T> implements Consumer<T>{

	private Logger logger = LoggerFactory.getLogger(AbstractConsumer.class);
	private ConsumerStatus status;
	protected BlockingQueue<T> queue = null;
	
	public AbstractConsumer(){
		status = ConsumerStatus.RUNNING;
	}
	
	public BlockingQueue<T> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<T> queue) {
		this.queue = queue;
	}

	public void run() {	
		while (ConsumerStatus.RUNNING.equals(status)) {
			try {
				process(queue.take());
			} catch (InterruptedException e){
				status = ConsumerStatus.INTERRUPTED;
				logger.error("Error. Interruption occure", e);
			} catch (Exception e) {
				logger.error("Error processing element", e);
			}
		}
	}
	
	public abstract void process(T elt)throws Exception;
}