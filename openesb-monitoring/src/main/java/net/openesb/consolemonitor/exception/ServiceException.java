package net.openesb.consolemonitor.exception;


/**
 * The service exception.	
 * 
 * @author loic.dassonville@ineat-conseil.com
 * 
 */
public class ServiceException extends Exception {

	/**
	 * The serial version.
	 */
	private static final long serialVersionUID = 7674038535749649600L;
	
	public ServiceException(String message) {
		super(message);
	}
	
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}