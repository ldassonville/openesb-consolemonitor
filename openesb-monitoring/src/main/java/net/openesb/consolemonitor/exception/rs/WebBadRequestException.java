package net.openesb.consolemonitor.exception.rs;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import net.openesb.consolemonitor.dto.ErrorDto;



/**
 * Exception corresponding to a bad user request
 * 
 * @author loic.dassonville@ineat-conseil.com
 * 
 */
public class WebBadRequestException extends WebApplicationException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -8282189170091978817L;

	public WebBadRequestException(String code, String message){
		super(Response.status(Response.Status.BAD_REQUEST)
				.entity(new ErrorDto(code,message))
				.build());		
	}
}