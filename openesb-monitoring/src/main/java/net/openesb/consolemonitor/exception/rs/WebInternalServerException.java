package net.openesb.consolemonitor.exception.rs;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import net.openesb.consolemonitor.dto.ErrorDto;



/**
 * The default internal server error
 * 
 * @author loic.dassonville
 * 
 */
public class WebInternalServerException extends WebApplicationException {


	/**
	 * serial UID
	 */
	private static final long serialVersionUID = 8263234069191954360L;

	public WebInternalServerException(Exception e){
		super(e, Response.serverError()
				.entity(new ErrorDto("INTERNAL_ERROR","Un unexpected error occured"))
				.build());		
	}
}