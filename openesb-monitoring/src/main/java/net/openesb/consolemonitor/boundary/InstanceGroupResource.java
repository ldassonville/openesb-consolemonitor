/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package net.openesb.consolemonitor.boundary;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import net.openesb.consolemonitor.boundary.mapper.InstanceGroupMapper;
import net.openesb.consolemonitor.boundary.mapper.InstanceMapper;
import net.openesb.consolemonitor.dto.InstanceDto;
import net.openesb.consolemonitor.dto.InstanceGroupDto;
import net.openesb.consolemonitor.exception.rs.WebInternalServerException;
import net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup;
import net.openesb.consolemonitor.service.itf.InstanceGroupService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import javax.ws.rs.core.Response;
/**
 * 
 * @author Loic DASSONVILLE 
 */
@Path("/group")
@Component
@Scope("prototype")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class InstanceGroupResource {

	private Logger logger = LoggerFactory.getLogger(InstanceGroupResource.class);
	
    @Context UriInfo uriInfo;
    
    @Autowired
    private InstanceGroupService instanceGroupService;
    
    @GET
    @Path("/{id}")
    public InstanceGroupDto getById(@PathParam("id") Long id)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting instance group by id {} ");

	    	InstanceGroup instanceGroup = instanceGroupService.getById(id);
	    	InstanceGroupMapper mapper = new InstanceGroupMapper(uriInfo.getBaseUri());
	    	return mapper.map(instanceGroup);

    	}catch(Exception e){
    		logger.error("Error getting instance group by id",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    /**
     * Give all clusters.
     * 
     * @throws WebApplicationException
     */
    @GET
    @Path("/")
    public List<InstanceGroupDto> getAll()
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting all instance groups {} ");
	    	List<InstanceGroup> instanceGroups = instanceGroupService.findAll();
	    	InstanceGroupMapper mapper = new InstanceGroupMapper(uriInfo.getBaseUri());
	    	return mapper.map(instanceGroups);
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting all instanceGroup",e);
    		throw new WebInternalServerException(e);
    	}
    } 

    /**
     * Give all instances.
     * 
     * @throws WebApplicationException
     */
    @GET
    @Path("/{id}/instances")
    public List<InstanceDto> getInstanceById(@PathParam("id") Long id)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting all instance by id {} ");
	    	InstanceGroup instanceGroup = instanceGroupService.getById(id);
	    	InstanceMapper mapper = new InstanceMapper(uriInfo.getBaseUri());
	    	return mapper.map(instanceGroup.getInstances());
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting all instanceGroup",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/")
    public Response save(@RequestBody InstanceGroupDto group)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Save instance group");

	    	InstanceGroupMapper mapper = new InstanceGroupMapper(uriInfo.getBaseUri());
	    	InstanceGroup instanceGroup = mapper.map(group);
	    	instanceGroup = instanceGroupService.save(instanceGroup);
	    	URI uri = uriInfo.getBaseUriBuilder().path(String.valueOf(instanceGroup.getId())).build();
	    	return Response.created(uri).build();
	    	
    	}catch(Exception e){
    		logger.error("Error getting instance group by id",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_HTML})
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Deleting group by id {} ");

	    	InstanceGroup instanceGroup = instanceGroupService.getById(id);
	    	instanceGroupService.delete(instanceGroup);
	    	return Response.ok().build();
	    	
    	}catch(Exception e){
    		logger.error("Error deleting group {}",id, e);
    		throw new WebInternalServerException(e);
    	}
    } 

}
