package net.openesb.consolemonitor.boundary.utils;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import net.openesb.consolemonitor.persistence.entity.Entity;


public final class PathUtils {

	private static Map<Entity<?>, String> patternCache = new HashMap<>();
	
	private static final String INSTANCE_GROUP_PATTERN = "%sgroup/%s";
	private static final String INSTANCE_GROUP_INSTANCE_PATTERN = INSTANCE_GROUP_PATTERN +"/instances";
	
	private static final String HOST_PATTERN = "%shost/%s";
	private static final String HOST_INSTANCE_PATTERN = HOST_PATTERN + "/instances";
	
	private static final String INSTANCE_PATTERN = "%sinstance/%s";
	private static final String INSTANCE_ENDPOINT_PATTERN = INSTANCE_PATTERN + "/endpoints";
	
	@SuppressWarnings("unused")
	private static final String INSTANCEGROUP_PATTERN = "%sgroup/%s";
	
	private static final String ENDPOINT_PATTERN = "%sendpoint/%s";
	private static final String ENDPOINT_STATISTICS_PATTERN = ENDPOINT_PATTERN + "/statistics";
	
	private static String resolvePattern(Entity<?> e) throws Exception {
	
		String patternName = e.getClass().getSimpleName().toUpperCase() +"_PATTERN";
		String patternValue = null;
		
		if(!patternCache.containsKey(e)){
			patternValue = ReflexionUtils.getStaticValue(PathUtils.class, patternName);
			patternCache.put(e, patternValue);
		}else{
			patternValue = patternCache.get(e);
		}
		return patternValue;
	}
	
	public static String getSelf(URI uri, Entity<?> e) throws Exception{	
		String pattern = resolvePattern(e);
		return String.format(pattern, uri.toString(), String.valueOf(e.getId()));
	}
	
	public static String getHostInstancesPath(URI uri, Long id){
		return String.format(HOST_INSTANCE_PATTERN, uri.toString(), id);
	}
	
	public static String getInstanceEndpointsPath(URI uri, Long id){
		return String.format(INSTANCE_ENDPOINT_PATTERN, uri.toString(), id);
	}

	public static String getEndpointStatisticsPath(URI uri, Long enpointId) {
		return String.format(ENDPOINT_STATISTICS_PATTERN, uri.toString(), enpointId);
	}

	public static String getInstanceGroupInstancePath(URI uri, Long instanceGroupId) {
		return String.format(INSTANCE_GROUP_INSTANCE_PATTERN, uri.toString(), instanceGroupId);
	}
}
