package net.openesb.consolemonitor.boundary.utils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public class ReflexionUtils {

	@SuppressWarnings("rawtypes")
	public static Class<?> getGeneric(Class<?> root, Class<?> lookingForClass, String genericName){
		
		int position = 0;
		TypeVariable[] variables = lookingForClass.getTypeParameters();
		for (TypeVariable typeVariable : variables) {
			if(genericName.equalsIgnoreCase(typeVariable.getName())){
				break;
			}
			position++;
		}
		
		int cpt = 0;
		Type typeSuperclass = root.getGenericSuperclass();
		if(typeSuperclass instanceof ParameterizedType){

			ParameterizedType parameterizedType = (ParameterizedType)typeSuperclass;
			Type[] types = parameterizedType.getActualTypeArguments();
			for (Type type : types) {
				if(cpt == position){
					return (Class<?>)type;
				}
				cpt++;
			}
		}		
		return null;	
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getStaticValue(Class<?> clazz, String attributeName)throws Exception{
		Field field = clazz.getDeclaredField(attributeName);
		boolean accessible = field.isAccessible();
		field.setAccessible(true);
		T res = (T) field.get(null);
		field.setAccessible(accessible);
		return res;
	}
	
}
