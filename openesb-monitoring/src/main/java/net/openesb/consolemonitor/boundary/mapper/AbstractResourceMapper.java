package net.openesb.consolemonitor.boundary.mapper;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.openesb.consolemonitor.boundary.utils.PathUtils;
import net.openesb.consolemonitor.boundary.utils.ReflexionUtils;
import net.openesb.consolemonitor.dto.ResourceDto;
import net.openesb.consolemonitor.persistence.entity.Entity;

import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

public abstract class AbstractResourceMapper<E extends Entity<?>, R extends ResourceDto> {

	protected URI uri; 
	
	public AbstractResourceMapper(URI uri){
		this.uri= uri;
	}
	
	public List<R> map(Collection<E> elts) throws Exception{
		List<R> res = new ArrayList<>();
		if(elts != null){
			for (E elt : elts) {
				res.add(map(elt));
			}
		}
		return res;
	}
	
    @SuppressWarnings("unchecked")
	public R map(E elt) throws Exception{
    	Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
    	R res = mapper.map(elt, (Class<R>)ReflexionUtils.getGeneric(getClass(), AbstractResourceMapper.class, "R"));
    	res.setSelf(PathUtils.getSelf(uri, elt));
    	return res;
    }

    @SuppressWarnings("unchecked")
	public E map(R elt) throws Exception{
    	Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
    	E res = mapper.map(elt, (Class<E>)ReflexionUtils.getGeneric(getClass(), AbstractResourceMapper.class, "E"));
    	return res;
    }
}
