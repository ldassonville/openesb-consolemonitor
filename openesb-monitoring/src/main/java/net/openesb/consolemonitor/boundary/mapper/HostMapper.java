package net.openesb.consolemonitor.boundary.mapper;

import java.net.URI;

import net.openesb.consolemonitor.boundary.utils.PathUtils;
import net.openesb.consolemonitor.dto.HostDto;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Host;

public class HostMapper extends AbstractResourceMapper<Host, HostDto>{

	public HostMapper(URI uri){
		super(uri);
	}
	
    public HostDto map(Host host) throws Exception{
    	HostDto hostDto = super.map(host);
		hostDto.setInstances(PathUtils.getHostInstancesPath(uri, host.getId()));
		return hostDto;
    }
}
