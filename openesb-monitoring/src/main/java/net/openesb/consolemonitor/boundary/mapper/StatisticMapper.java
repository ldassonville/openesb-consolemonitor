package net.openesb.consolemonitor.boundary.mapper;

import java.net.URI;

import net.openesb.consolemonitor.dto.StatisticDto;
import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;

import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

public class StatisticMapper extends AbstractResourceMapper<EndpointStatistic, StatisticDto>{

	public StatisticMapper(URI uri){
		super(uri);
	}
	
    public StatisticDto map(EndpointStatistic statistic){
    	Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
    	StatisticDto res = mapper.map(statistic, StatisticDto.class);
		return res;
    }
}
