/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package net.openesb.consolemonitor.boundary;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import net.openesb.consolemonitor.boundary.mapper.EndpointMapper;
import net.openesb.consolemonitor.boundary.mapper.StatisticMapper;
import net.openesb.consolemonitor.dto.EndpointDto;
import net.openesb.consolemonitor.dto.StatisticDto;
import net.openesb.consolemonitor.exception.rs.WebInternalServerException;
import net.openesb.consolemonitor.persistence.entity.Endpoint;
import net.openesb.consolemonitor.service.itf.EndpointService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * This resource provide REST services for statistic entity.
 * 
 * @author loic.dassonville
 */
@Path("/endpoint")
@Component
@Scope("prototype")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class EndpointResource {
	
	private Logger logger = LoggerFactory.getLogger(EndpointResource.class);
	
    @Context UriInfo uriInfo;
    
    @Autowired
    private EndpointService endpointService;
  
    @GET
    @Path("/{id}")
    public EndpointDto getById(@PathParam("id") Long id)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting endpoint by id {} ");

	    	Endpoint endpoint = endpointService.getById(id);
	    	EndpointMapper mapper = new EndpointMapper(uriInfo.getBaseUri());
    		return mapper.map(endpoint);
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting endpoint by id",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    /**
     * Give all hosts.
     * 
     * @throws WebApplicationException
     */
    @GET
    @Path("/")
    public List<EndpointDto> getAll()
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting all enpoints {} ");
	    	
	    	List<Endpoint> endpoints = endpointService.findAll();
	    	EndpointMapper mapper = new EndpointMapper(uriInfo.getBaseUri());
    		return mapper.map(endpoints);
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting all endpoints",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    @GET
    @Path("/{id}/statistics")
    public List<StatisticDto> getStatisticsById(@PathParam("id") Long id)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting endpoint statistics by id {} ");

	    	Endpoint endpoint = endpointService.getById(id);
	    	StatisticMapper mapper = new StatisticMapper(uriInfo.getBaseUri());
    		List<StatisticDto> res = mapper.map(endpoint.getStatistics());
    		Collections.sort(res, new EndpointStatisticDateComparator());
    		return res;
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting endpoint statistics by id",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    private class EndpointStatisticDateComparator implements Comparator<StatisticDto>{

		@Override
		public int compare(StatisticDto o1, StatisticDto o2) {
			if(o1 == null || o1.getDate() == null) return -1;
			if(o2 == null) return 1;
				
			return o1.getDate().compareTo(o2.getDate());
		}
    	
    }
 
}
