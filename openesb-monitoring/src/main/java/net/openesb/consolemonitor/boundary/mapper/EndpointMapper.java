package net.openesb.consolemonitor.boundary.mapper;

import java.net.URI;

import net.openesb.consolemonitor.boundary.utils.PathUtils;
import net.openesb.consolemonitor.dto.EndpointDto;
import net.openesb.consolemonitor.persistence.entity.Endpoint;

public class EndpointMapper extends AbstractResourceMapper<Endpoint, EndpointDto>{

	public EndpointMapper(URI uri){
		super(uri);
	}
	
    public EndpointDto map(Endpoint endpoint) throws Exception{
    	EndpointDto endpointDto = super.map(endpoint);
		endpointDto.setInstance(PathUtils.getSelf(uri, endpoint.getInstance()));
		endpointDto.setStatistics(PathUtils.getEndpointStatisticsPath(uri, endpoint.getId()));
		return endpointDto;
    }
}
