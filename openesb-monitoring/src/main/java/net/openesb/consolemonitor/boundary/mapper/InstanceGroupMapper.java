package net.openesb.consolemonitor.boundary.mapper;

import java.net.URI;

import net.openesb.consolemonitor.boundary.utils.PathUtils;
import net.openesb.consolemonitor.dto.InstanceGroupDto;
import net.openesb.consolemonitor.persistence.entity.infrastructure.InstanceGroup;

public class InstanceGroupMapper extends AbstractResourceMapper<InstanceGroup, InstanceGroupDto>{

	public InstanceGroupMapper(URI uri){
		super(uri);
	}
	
    public InstanceGroupDto map(InstanceGroup instanceGroup) throws Exception{
    	InstanceGroupDto instanceGroupDto = super.map(instanceGroup);
    	instanceGroupDto.setInstances(PathUtils.getInstanceGroupInstancePath(uri, instanceGroup.getId()));
		return instanceGroupDto;   
	}
    
    public InstanceGroup map(InstanceGroupDto instanceGroupDto) throws Exception{
    	InstanceGroup instanceGroup = super.map(instanceGroupDto);
		return instanceGroup;   
	}
}
