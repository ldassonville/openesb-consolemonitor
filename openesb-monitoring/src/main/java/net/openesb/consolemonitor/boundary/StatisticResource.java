/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package net.openesb.consolemonitor.boundary;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import net.openesb.consolemonitor.boundary.mapper.StatisticMapper;
import net.openesb.consolemonitor.dto.StatisticDto;
import net.openesb.consolemonitor.exception.rs.WebInternalServerException;
import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;
import net.openesb.consolemonitor.service.itf.StatisticService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * This resource provide REST services for statistic entity.
 * 
 * @author Loic Dassonville
 */
@Path("/statistic/")
@Component
@Scope("prototype")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class StatisticResource {

	private Logger logger = LoggerFactory.getLogger(StatisticResource.class);
	
    @Context UriInfo uriInfo;
    
    @Autowired
    private StatisticService statisticService;
    
    /**
     * Give statistics.
     * 
     * @throws WebApplicationException
     */
    @GET
    @Path("/")
    public List<StatisticDto> findAll()
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting all statistics {} ");
	    	
	    	StatisticMapper mapper = new StatisticMapper(uriInfo.getBaseUri());
	    	List<EndpointStatistic> statistics = statisticService.findAll();
	    	return mapper.map(statistics);
	    	
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting all statisitics",e);
    		throw new WebInternalServerException(e);
    	}
    } 

}
