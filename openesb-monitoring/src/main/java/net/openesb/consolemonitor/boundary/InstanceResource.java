/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package net.openesb.consolemonitor.boundary;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import net.openesb.consolemonitor.boundary.mapper.EndpointMapper;
import net.openesb.consolemonitor.boundary.mapper.InstanceMapper;
import net.openesb.consolemonitor.dto.EndpointDto;
import net.openesb.consolemonitor.dto.InstanceDto;
import net.openesb.consolemonitor.exception.rs.WebInternalServerException;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;
import net.openesb.consolemonitor.service.itf.InstanceService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * This resource provide REST services for statistic entity.
 * 
 * @author loic.dassonville
 */
@Path("/instance")
@Component
@Scope("prototype")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class InstanceResource {
	
	private Logger logger = LoggerFactory.getLogger(InstanceResource.class);
	
    @Context UriInfo uriInfo;
    
    @Autowired
    private InstanceService instanceService;
  
    @GET
    @Path("/{id}")
    public InstanceDto getById(@PathParam("id") Long id)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting instance by id {} ");

	    	Instance instance = instanceService.getById(id);
	    	InstanceMapper mapper = new InstanceMapper(uriInfo.getBaseUri());
	    	return mapper.map(instance);
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting instance by id",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    @GET
    @Path("/name/{name}")
    public InstanceDto getByName(@PathParam("name") String name)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting instance by name {} ");

	    	Instance instance = instanceService.getByName(name);
	    	InstanceMapper mapper = new InstanceMapper(uriInfo.getBaseUri());
	    	return mapper.map(instance);
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting instance by id",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    /**
     * Give all hosts.
     * 
     * @throws WebApplicationException
     */
    @GET
    @Path("/")
    public List<InstanceDto> getAll()
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting all instances {} ");
	    	
	    	List<Instance> instances = instanceService.findAll();
	    	InstanceMapper mapper = new InstanceMapper(uriInfo.getBaseUri());
	    	return mapper.map(instances);
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting all instances",e);
    		throw new WebInternalServerException(e);
    	}
    } 
    
    @GET
    @Path("/{id}/endpoints")
    public List<EndpointDto> getEnpointById(@PathParam("id") Long id)
    		throws WebApplicationException {
    
    	try{
	    	logger.info("Getting instance endpoint by id {} ");

	    	Instance instance = instanceService.getById(id);
	    	EndpointMapper mapper = new EndpointMapper(uriInfo.getBaseUri());
	    	return mapper.map(instance.getEndpoints());
    		
    	}catch(WebApplicationException e){
    		throw e;
    	}catch(Exception e){
    		logger.error("Error getting instance endoint by id",e);
    		throw new WebInternalServerException(e);
    	}
    } 

}
