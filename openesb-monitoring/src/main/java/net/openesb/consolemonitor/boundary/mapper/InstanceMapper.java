package net.openesb.consolemonitor.boundary.mapper;

import java.net.URI;

import net.openesb.consolemonitor.boundary.utils.PathUtils;
import net.openesb.consolemonitor.dto.InstanceDto;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;

public class InstanceMapper extends AbstractResourceMapper<Instance, InstanceDto>{

	public InstanceMapper(URI uri){
		super(uri);
	}
	
    public InstanceDto map(Instance instance) throws Exception{
    	InstanceDto instanceDto = super.map(instance);
		instanceDto.setInstanceGroup(PathUtils.getSelf(uri, instance.getInstanceGroup()));
		instanceDto.setEndpoints(PathUtils.getInstanceEndpointsPath(uri, instance.getId()));		
		return instanceDto;
    }
}
