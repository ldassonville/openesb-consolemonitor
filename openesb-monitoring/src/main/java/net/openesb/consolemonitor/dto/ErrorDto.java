package net.openesb.consolemonitor.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Loic Dassonville
 */
@XmlRootElement
public class ErrorDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code;
	private String message;
	
	public ErrorDto(){}
	
	public ErrorDto(String code, String msg){
		this.code = code;
		this.message = msg;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
