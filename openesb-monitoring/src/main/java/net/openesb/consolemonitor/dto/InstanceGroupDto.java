/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package net.openesb.consolemonitor.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Loic Dassonville
 */
@XmlRootElement
public class InstanceGroupDto extends ResourceDto  {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String instances;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstances() {
		return instances;
	}

	public void setInstances(String instances) {
		this.instances = instances;
	}
}
