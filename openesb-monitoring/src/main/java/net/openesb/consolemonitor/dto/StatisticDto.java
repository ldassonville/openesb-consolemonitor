/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package net.openesb.consolemonitor.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Loic Dassonville
 */
@XmlRootElement
public class StatisticDto extends ResourceDto {

	private static final long serialVersionUID = 1L;

	private Date date;
	private Date activationTimestamp;
	private Long activeExchanges;
	private Date lastDONETime;
	private Date lastERRORTime;
	private Date lastFaultTime;
	private String owningChannel;
	private Long receiveDONE;
	private Long receiveERROR;
	private Long receiveFault;
	private Long receiveReply;
	private Long receiveRequest;
	private Long sendDONE;
	private Long sendERROR;
	private Long sendFault;
	private Long sendReply;
	private Long sendRequest;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getActivationTimestamp() {
		return activationTimestamp;
	}
	public void setActivationTimestamp(Date activationTimestamp) {
		this.activationTimestamp = activationTimestamp;
	}
	public Long getActiveExchanges() {
		return activeExchanges;
	}
	public void setActiveExchanges(Long activeExchanges) {
		this.activeExchanges = activeExchanges;
	}
	public Date getLastDONETime() {
		return lastDONETime;
	}
	public void setLastDONETime(Date lastDONETime) {
		this.lastDONETime = lastDONETime;
	}
	public Date getLastERRORTime() {
		return lastERRORTime;
	}
	public void setLastERRORTime(Date lastERRORTime) {
		this.lastERRORTime = lastERRORTime;
	}
	public Date getLastFaultTime() {
		return lastFaultTime;
	}
	public void setLastFaultTime(Date lastFaultTime) {
		this.lastFaultTime = lastFaultTime;
	}
	public String getOwningChannel() {
		return owningChannel;
	}
	public void setOwningChannel(String owningChannel) {
		this.owningChannel = owningChannel;
	}
	public Long getReceiveDONE() {
		return receiveDONE;
	}
	public void setReceiveDONE(Long receiveDONE) {
		this.receiveDONE = receiveDONE;
	}
	public Long getReceiveERROR() {
		return receiveERROR;
	}
	public void setReceiveERROR(Long receiveERROR) {
		this.receiveERROR = receiveERROR;
	}
	public Long getReceiveFault() {
		return receiveFault;
	}
	public void setReceiveFault(Long receiveFault) {
		this.receiveFault = receiveFault;
	}
	public Long getReceiveReply() {
		return receiveReply;
	}
	public void setReceiveReply(Long receiveReply) {
		this.receiveReply = receiveReply;
	}
	public Long getReceiveRequest() {
		return receiveRequest;
	}
	public void setReceiveRequest(Long receiveRequest) {
		this.receiveRequest = receiveRequest;
	}
	public Long getSendDONE() {
		return sendDONE;
	}
	public void setSendDONE(Long sendDONE) {
		this.sendDONE = sendDONE;
	}
	public Long getSendERROR() {
		return sendERROR;
	}
	public void setSendERROR(Long sendERROR) {
		this.sendERROR = sendERROR;
	}
	public Long getSendFault() {
		return sendFault;
	}
	public void setSendFault(Long sendFault) {
		this.sendFault = sendFault;
	}
	public Long getSendReply() {
		return sendReply;
	}
	public void setSendReply(Long sendReply) {
		this.sendReply = sendReply;
	}
	public Long getSendRequest() {
		return sendRequest;
	}
	public void setSendRequest(Long sendRequest) {
		this.sendRequest = sendRequest;
	}
	
	
}
