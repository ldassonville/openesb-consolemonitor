package net.openesb.consolemonitor.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author loic.dassonville
 */
@XmlRootElement
@JsonInclude(Include.NON_NULL)
public class HostDto extends ResourceDto {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private String instances;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstances() {
		return instances;
	}

	public void setInstances(String instances) {
		this.instances = instances;
	}

}
