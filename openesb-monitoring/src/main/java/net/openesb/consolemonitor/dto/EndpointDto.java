package net.openesb.consolemonitor.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Loic Dassonville
 */
@XmlRootElement
public class EndpointDto extends ResourceDto {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String instance;
	private String statistics;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getStatistics() {
		return statistics;
	}

	public void setStatistics(String statistics) {
		this.statistics = statistics;
	}
}
