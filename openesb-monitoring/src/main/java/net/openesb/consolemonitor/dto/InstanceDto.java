package net.openesb.consolemonitor.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Loic Dassonville
 */
@XmlRootElement
public class InstanceDto extends ResourceDto {

	private static final long serialVersionUID = 1L;

	private String name;
	private String instanceGroup;
	private String endpoints;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstanceGroup() {
		return instanceGroup;
	}

	public void setInstanceGroup(String instanceGroup) {
		this.instanceGroup = instanceGroup;
	}

	public String getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(String endpoints) {
		this.endpoints = endpoints;
	}
}
