
--
-- TOC entry 2206 (class 0 OID 20050)
-- Dependencies: 168
-- Data for Name: cluster; Type: TABLE DATA; Schema: public; Owner: openesb-monitoring
--

insert into cluster (id, name) values (9, 'produit');
insert into cluster (id, name) values (10, 'client');
insert into cluster (id, name) values (11, 'VAD');



--
-- TOC entry 2208 (class 0 OID 20063)
-- Dependencies: 170
-- Data for Name: host; Type: TABLE DATA; Schema: public; Owner: openesb-monitoring
--

insert into host (id, name) values (1, 'adwbs-li51');
insert into host (id, name) values (2, 'adwbs-li52');
insert into host (id, name) values (3, 'rfrlmasweba04');
insert into host (id, name) values (4, 'rfrlmasweba06');
insert into host (id, name) values (5, 'dfrlmasweba04');
insert into host (id, name) values (6, 'dfrlmasweba06');
insert into host (id, name) values (7, 'ffrlmaswebf01');
insert into host (id, name) values (8, 'ffrlmaswebf02');



--
-- TOC entry 2209 (class 0 OID 20068)
-- Dependencies: 171
-- Data for Name: node; Type: TABLE DATA; Schema: public; Owner: openesb-monitoring
--


insert into node (id, jmxport, name, cluster_id, host_id) values (12,	38686,	'produit-adwbs-li51',	9,	1);
insert into node (id, jmxport, name, cluster_id, host_id) values (13,	38686,  'produit-adwbs-li52',	9,	2);
insert into node (id, jmxport, name, cluster_id, host_id) values (14,	38686,	'produit-rfrlmasweba04',	9,	3);
insert into node (id, jmxport, name, cluster_id, host_id) values (15,	38686,	'produit-rfrlmasweba06',	9,	4);
insert into node (id, jmxport, name, cluster_id, host_id) values (16,	38686,	'produit-dfrlmasweba04',	9,	5);
insert into node (id, jmxport, name, cluster_id, host_id) values (17,	38686,	'produit-dfrlmasweba06',	9,	6);
insert into node (id, jmxport, name, cluster_id, host_id) values (18,	38686,	'produit-ffrlmaswebf01',	9,	7);
insert into node (id, jmxport, name, cluster_id, host_id) values (19,	38686,	'produit-ffrlmaswebf02',	9,	8);
insert into node (id, jmxport, name, cluster_id, host_id) values (20,	38687,	'client-adwbs-li51',	10,	1);
insert into node (id, jmxport, name, cluster_id, host_id) values (21,	38687,	'client-adwbs-li52',	10,	2);
insert into node (id, jmxport, name, cluster_id, host_id) values (22,	38687,	'client-rfrlmasweba04',	10,	3);
insert into node (id, jmxport, name, cluster_id, host_id) values (23,	38687,	'client-rfrlmasweba06',	10,	4);
insert into node (id, jmxport, name, cluster_id, host_id) values (24,	38687,	'client-dfrlmasweba04',	10,	5);
insert into node (id, jmxport, name, cluster_id, host_id) values (25,	38687,	'client-dfrlmasweba06',	10,	6);
insert into node (id, jmxport, name, cluster_id, host_id) values (26,	38687,	'client-ffrlmaswebf01',	10,	7);
insert into node (id, jmxport, name, cluster_id, host_id) values (27,	38687,	'client-ffrlmaswebf02',	10,	8);
insert into node (id, jmxport, name, cluster_id, host_id) values (28,	38688,	'VAD-adwbs-li51',	11,	1);
insert into node (id, jmxport, name, cluster_id, host_id) values (29,	38688,	'VAD-adwbs-li52',	11,	2);
insert into node (id, jmxport, name, cluster_id, host_id) values (30,	38688,	'VAD-rfrlmasweba04',	11,	3);
insert into node (id, jmxport, name, cluster_id, host_id) values (31,	38688,	'VAD-rfrlmasweba06',	11,	4);
insert into node (id, jmxport, name, cluster_id, host_id) values (32,	38688,	'VAD-dfrlmasweba04',	11,	5);
insert into node (id, jmxport, name, cluster_id, host_id) values (33,	38688,	'VAD-dfrlmasweba06',	11,	6);
insert into node (id, jmxport, name, cluster_id, host_id) values (34,	38688,	'VAD-ffrlmaswebf01',	11,	7);
insert into node (id, jmxport, name, cluster_id, host_id) values (35,	38688,	'VAD-ffrlmaswebf02',	11,	8);
