package net.openesb.consolemonitor.service;


import java.util.List;

import junit.framework.Assert;

import net.openesb.consolemonitor.persistence.entity.Endpoint;
import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;
import net.openesb.consolemonitor.persistence.entity.statistic.ProviderEndpointStatistic;
import net.openesb.consolemonitor.service.itf.StatisticService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext-repository.xml",
									"classpath:spring/applicationContext-service.xml" })
public class StatisticRespositoryTest {
	
	@Autowired StatisticService statisticService;

	
	@Test
	public void saveStatistic_Test() {
		try {
			EndpointStatistic statistic = new ProviderEndpointStatistic();
			Endpoint endpoint = new Endpoint();
			endpoint.setName("endpointName");
			
			statistic.setEndpoint(endpoint);
			statistic.setOwningChannel("sun-http-binding");
			statistic.setReceiveDONE(789l);
			statistic = statisticService.save(statistic);
			
			Assert.assertNotNull(statistic.getId());
			
			List<EndpointStatistic> endpointStatistics = statisticService.findAll();
			System.out.println(endpointStatistics.size());
			System.out.println(endpointStatistics.get(0).getId());
			System.out.println(endpointStatistics.get(0).getOwningChannel());
			System.out.println(endpointStatistics.get(0).getReceiveDONE());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
