package net.openesb.consolemonitor.service;


import java.util.List;

import junit.framework.Assert;

import net.openesb.consolemonitor.persistence.entity.criteria.InstanceCriteria;
import net.openesb.consolemonitor.persistence.entity.infrastructure.Instance;
import net.openesb.consolemonitor.persistence.entity.statistic.EndpointStatistic;
import net.openesb.consolemonitor.persistence.entity.statistic.ProviderEndpointStatistic;
import net.openesb.consolemonitor.service.itf.InstanceService;
import net.openesb.consolemonitor.service.itf.StatisticService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext-repository.xml",
									"classpath:spring/applicationContext-service.xml" })
public class NodeServiceTest {
	
	@Autowired InstanceService nodeService;

	
//	@Test
//	public void searchNode_Test() {
//		try {
//			
//			InstanceCriteria criteria = new InstanceCriteria();
//			criteria.setInstanceGroupName("VAD");
//			
//			List<Instance> nodes  = nodeService.searchNode(criteria);
//			System.out.println(nodes.size());
//			Assert.assertNotNull(nodes);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
